import allure
import pytest
from config.links import Links
from data.data_generator import DataGenerator
from pages.inputs_page import TextInputPage, EmailFieldPage, PasswordFieldPage


class TestInput:
    PAGE_URL = Links.INPUT_TEXT
    randon_text_generator = DataGenerator()

    @allure.feature('Text Input')
    @allure.story('Valid text input check')
    @pytest.mark.parametrize('random_text', [
        (randon_text_generator.generator_random_text(2)),
        (randon_text_generator.generator_random_text(5)),
        (randon_text_generator.generator_random_text(10)),
        (randon_text_generator.generator_random_text(15)),
        (randon_text_generator.generator_random_text(25))
    ], ids=['random_text_1', 'random_text_2', 'random_text_3', 'random_text_4', 'random_text_5'])
    def test_text_input(self, driver, random_text):
        text_input_page = TextInputPage(driver, self.PAGE_URL)
        text_input_page.open()
        text_input_page.is_opened(self.PAGE_URL)
        entered_text = text_input_page.enter_text(random_text)
        with allure.step('Checking that the text entered by the user is displayed on the page'):
            assert entered_text == random_text, 'The text entered by the user is not displayed on the page'

    @allure.feature('Text Input')
    @allure.story('Invalid text input check')
    @pytest.mark.parametrize('random_text, output_text, output_text_2', [
        (randon_text_generator.generator_random_text(1), 'Please enter 2 or more characters', None),
        (randon_text_generator.generator_random_text(26), 'Please enter no more than 25 characters', None),
        (randon_text_generator.generator_negative_random_text(10), 'Enter a valid string consisting of letters, '
                                                                   'numbers, underscores or hyphens.', None),
        (randon_text_generator.generator_negative_random_text(1), 'Please enter 2 or more characters',
         'Enter a valid string consisting of letters, numbers, underscores or hyphens.'),
        (randon_text_generator.generator_negative_random_text(26), 'Please enter no more than 25 characters',
         'Enter a valid string consisting of letters, numbers, underscores or hyphens.')
    ], ids=['random_text_1', 'random_text_2', 'random_text_3', 'random_text_4', 'random_text_5'])
    def test_input_negative(self, driver, random_text, output_text, output_text_2):
        text_input_page = TextInputPage(driver, self.PAGE_URL)
        text_input_page.open()
        text_input_page.is_opened(self.PAGE_URL)
        invalid_text, invalid_text_2 = text_input_page.negative_enter_text(random_text)
        with allure.step("Checking that the user has not entered invalid text"):
            assert invalid_text == output_text, 'Invalid text input allowed'
        if output_text_2 is None:
            with allure.step("Checking that the user has not entered invalid text"):
                assert invalid_text_2 is None, 'Invalid text input allowed'
        else:
            with allure.step("Checking that the user has not entered invalid text"):
                assert invalid_text_2 == output_text_2, 'Invalid text input allowed'


class TestEmailField:
    PAGE_URL = Links.EMAIL_FIELD
    email_generator = DataGenerator()

    @allure.feature('Email field')
    @allure.story('Valid email input check')
    @pytest.mark.parametrize('email', [
        (email_generator.generator_email()),
        (email_generator.generator_email()),
        (email_generator.generator_email()),
        (email_generator.generator_email()),
        (email_generator.generator_email())
    ], ids=['email_1', 'email_2', 'email_3', 'email_4', 'email_5'])
    def test_email_input(self, driver, email):
        email_field_page = EmailFieldPage(driver, self.PAGE_URL)
        email_field_page.open()
        email_field_page.is_opened(self.PAGE_URL)
        entered_email = email_field_page.enter_email(email)
        with allure.step("Checking the display of the email entered by the user"):
            assert entered_email == email, 'The email entered by the user is not displayed on the page'

    @allure.feature('Email field')
    @allure.story('Invalid email input check')
    @pytest.mark.parametrize('random_text', [
        (email_generator.generator_random_text(1)),
        (email_generator.generator_random_text(10)),
        (email_generator.generator_random_text(25)),
        (email_generator.generator_random_text(30))
    ], ids=['random_text_1', 'random_text_2', 'random_text_3', 'random_text_4'])
    def test_invalid_email_input(self, driver, random_text):
        email_field_page = EmailFieldPage(driver, self.PAGE_URL)
        email_field_page.open()
        email_field_page.is_opened(self.PAGE_URL)
        entered_invalid_email_text = email_field_page.enter_invalid_email(random_text)
        with allure.step("Checking that the user has not entered invalid email"):
            assert entered_invalid_email_text == 'Enter a valid email address.', 'Invalid email allowed'


class TestPasswordField:
    PAGE_URL = Links.PASSWORD_FIELD
    password_generator = DataGenerator()

    @allure.feature('Password field')
    @allure.story('Valid password input check')
    @pytest.mark.parametrize('password', [
        (password_generator.generator_password(8)),
        (password_generator.generator_password(13)),
        (password_generator.generator_password(25))
    ], ids=['password_1', 'password_2', 'password_3'])
    def test_password_input(self, driver, password):
        password_field_page = PasswordFieldPage(driver, self.PAGE_URL)
        password_field_page.open()
        password_field_page.is_opened(self.PAGE_URL)
        entered_password = password_field_page.enter_password(password)
        with allure.step("Checking the display of the password entered by the user"):
            assert entered_password == password, 'The password entered by the user is not displayed on the page'

    @allure.feature('Password field')
    @allure.story('Invalid password input check')
    @pytest.mark.parametrize('invalid_password', [
        (password_generator.generator_password(7, special_chars=True, digits=True, upper_case=True, lower_case=True)),
        (password_generator.generator_password(10, special_chars=False, digits=True, upper_case=True, lower_case=True)),
        (password_generator.generator_password(13, special_chars=True, digits=False, upper_case=True, lower_case=True)),
        (password_generator.generator_password(20, special_chars=True, digits=True, upper_case=False, lower_case=True)),
        (password_generator.generator_password(25, special_chars=True, digits=True, upper_case=True, lower_case=False)),
    ], ids=['invalid_password_1', 'invalid_password_2', 'invalid_password_3', 'invalid_password_4', 'invalid_password_5'])
    def test_invalid_password_input(self, driver, invalid_password):
        password_field_page = PasswordFieldPage(driver, self.PAGE_URL)
        password_field_page.open()
        password_field_page.is_opened(self.PAGE_URL)
        entered_invalid_password_text = password_field_page.enter_invalid_password(invalid_password)
        with allure.step("Checking that the user has not entered invalid password"):
            assert entered_invalid_password_text == 'Low password complexity', 'Allowed entry of invalid password'
