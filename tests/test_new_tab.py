from config.links import Links
from pages.new_tab_page import NewTabPage


class TestNewTabLink:
    PAGE_URL = Links.NEW_TAB_LINK, Links.NEW_TAB_BUTTON
    NEW_TAB_PAGE_URL = Links.NEW_PAGE

    def test_check_opening_link(self, driver):
        new_tab_link_page = NewTabPage(driver, self.PAGE_URL[0])
        new_tab_link_page.open()
        new_tab_link_page.is_opened(self.PAGE_URL[0])
        new_tab_link_page.click_new_tab_link()
        new_tab_link_page.switching_to_a_new_tab()
        new_tab_link_page.check_new_tab_link(self.NEW_TAB_PAGE_URL)

    def test_check_opening_button(self, driver):
        new_tab_link_page = NewTabPage(driver, self.PAGE_URL[1])
        new_tab_link_page.open()
        new_tab_link_page.is_opened(self.PAGE_URL[1])
        new_tab_link_page.click_new_tab_button()
        new_tab_link_page.switching_to_a_new_tab()
        new_tab_link_page.check_new_tab_link(self.NEW_TAB_PAGE_URL)

