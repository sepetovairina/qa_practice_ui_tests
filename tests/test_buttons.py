import allure
from config.links import Links
from pages.buttons_page import SimpleButtonPage, LooksLikeAButtonPage, DisabledButtonPage


class TestSimpleButton:
    PAGE_URL = Links.SIMPLE_BUTTON

    @allure.feature('Simple Button')
    @allure.story('Checking the pressing of a simple button')
    def test_click_for_simple_button(self, driver):
        simple_button_page = SimpleButtonPage(driver, self.PAGE_URL)
        simple_button_page.open()
        simple_button_page.is_opened(self.PAGE_URL)
        text_simple_button, result_text = simple_button_page.click_simple_button()
        with allure.step('Checking that the button says "Click"'):
            assert text_simple_button == 'Click', 'The button does not have the inscription "Click"'
        with allure.step('Checking that after pressing the button the confirmation text "Submitted" appears'):
            assert result_text == 'Submitted', ('After pressing a button there is no text confirming that the button '
                                                'was pressed')


class TestLooksLikeAButton:
    PAGE_URL = Links.LOOKS_LIKE_A_BUTTON

    @allure.feature('Looks Like a Button')
    @allure.story('Checking the pressing for looks like a button')
    def test_click_for_looks_like_a_button(self, driver):
        looks_like_a_button_page = LooksLikeAButtonPage(driver, self.PAGE_URL)
        looks_like_a_button_page.open()
        looks_like_a_button_page.is_opened(self.PAGE_URL)
        text_looks_like_a_button, result_text = looks_like_a_button_page.click_looks_like_a_button()
        with allure.step('Checking that the button says "Click"'):
            assert text_looks_like_a_button == 'Click', 'The button does not have the inscription "Click"'
        with allure.step('Checking that after pressing the button the confirmation text "Submitted" appears'):
            assert result_text == 'Submitted', ('After pressing a button there is no text confirming that the button '
                                                'was pressed')


class TestDisabledButton:
    PAGE_URL = Links.DISABLED_BUTTON

    @allure.feature('Disabled Button')
    @allure.story('Checking an inactive button')
    def test_click_for_disabled_button(self, driver):
        disabled_state_button_page = DisabledButtonPage(driver, self.PAGE_URL)
        disabled_state_button_page.open()
        disabled_state_button_page.is_opened(self.PAGE_URL)
        select_state_attribute, select_state_text, submit_state = disabled_state_button_page.check_disabled_button()
        with allure.step('Checking for missing button state selection attribute'):
            assert select_state_attribute is None, 'There is a selection attribute from the dropdown list'
        with allure.step('Checking that the button state is "disabled"'):
            assert select_state_text == 'Disabled', '"Disabled" is not selected from the drop-down list'
        with allure.step('Checking that the "Submit" button has the attribute "disabled"'):
            assert submit_state == 'true', 'The "Submit" button does not have the "disabled" attribute'

    @allure.feature('Disabled Button')
    @allure.story('Checking the pressing of the active button')
    def test_submit_for_enabled_button(self, driver):
        enabled_state_button_page = DisabledButtonPage(driver, self.PAGE_URL)
        enabled_state_button_page.open()
        enabled_state_button_page.is_opened(self.PAGE_URL)
        result_text = enabled_state_button_page.click_enabled_button()
        with allure.step('Checking that after pressing the button there is a confirmation text "Submitted"'):
            assert result_text == 'Submitted', ('After pressing a button there is no text confirming that the button '
                                                'was pressed')


