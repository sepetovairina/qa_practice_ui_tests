import itertools
import allure
import pytest
from config.links import Links
from pages.checkbox_page import SingleCheckboxPage, CheckboxesPage


class TestSingleCheckbox:
    PAGE_URL = Links.SINGLE_CHECKBOX

    @allure.feature('Single Checkbox')
    @allure.story('Checking the pressed checkbox')
    def test_checkbox_on(self, driver):
        single_checkbox_page = SingleCheckboxPage(driver, self.PAGE_URL)
        single_checkbox_page.open()
        single_checkbox_page.is_opened(self.PAGE_URL)
        checkbox_text, result_text = single_checkbox_page.check_checkbox_on()
        with allure.step('Checking whether the selected checkbox has the text "Select me or not"'):
            assert 'Select me or not' in checkbox_text, 'The checkbox label does not match "Select me or not"'
        with allure.step('Checking whether the result matches the checkbox text'):
            assert result_text == 'select me or not', 'Name of the selected checkbox does not match "select me or not"'

    @allure.feature('Single Checkbox')
    @allure.story('Checking the un-pressed checkbox')
    def test_checkbox_off(self, driver):
        single_checkbox_page = SingleCheckboxPage(driver, self.PAGE_URL)
        single_checkbox_page.open()
        single_checkbox_page.is_opened(self.PAGE_URL)
        checkbox_text, element_result = single_checkbox_page.check_checkbox_off()
        with allure.step('Checking whether the selected checkbox has the text "Select me or not"'):
            assert 'Select me or not' in checkbox_text, 'The checkbox label does not match "Select me or not"'
        with allure.step('Checking that the result is not displayed when the checkbox is not selected'):
            assert len(element_result) == 0, 'The result is displayed when the checkbox is unchecked'


class TestCheckboxes:
    PAGE_URL = Links.CHECKBOXES

    @allure.feature('Checkboxes')
    @allure.story('Selecting all combinations of checkboxes')
    @pytest.mark.parametrize('number, checkboxes_indexes', [
        (0, combo) for count in range(1, 4) for combo in itertools.combinations(range(3), count)
    ])
    def test_checkboxes_combinations(self, driver, number, checkboxes_indexes):
        checkboxes_page = CheckboxesPage(driver, self.PAGE_URL)
        checkboxes_page.open()
        checkboxes_page.is_opened(self.PAGE_URL)
        checkbox_names, result_text = checkboxes_page.click_on_the_checkboxes(number, checkboxes_indexes)
        with allure.step('Checking that the result matches the selected checkboxes'):
            assert result_text == checkbox_names
