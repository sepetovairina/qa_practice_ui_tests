import itertools
from data.data_generator import place_names, how_names, when_names, format_test_name
import allure
import pytest
from config.links import Links
from pages.select_inputs_page import SingleSelectInputPage, MultipleSelectsInputPage


class TestSelectInputs:
    PAGE_URL = Links.SINGLE_SELECT

    @allure.feature('Single select')
    @allure.story('Check choose select')
    @pytest.mark.parametrize('select_number', [1, 2, 3, 4, 5], ids=['Python', 'Ruby', 'JavaScript', 'Java', 'C#'])
    def test_checking_single_select(self, driver, select_number):
        single_selector_page = SingleSelectInputPage(driver, self.PAGE_URL)
        single_selector_page.open()
        single_selector_page.is_opened(self.PAGE_URL)
        selected_text = single_selector_page.choice_select(select_number)
        result_text = single_selector_page.get_result_text()
        with allure.step('Checking that the result matches the selected option'):
            assert result_text == selected_text


class TestMultipleSelectsInputs:
    PAGE_URL = Links.MULTIPLE_SELECTS

    @allure.feature('Multiple selects')
    @allure.story('Check choose multiple select')
    @pytest.mark.parametrize("place, how, when", [
        (x, y, z)
        for x in list(place_names.keys())
        for y in list(how_names.keys())
        for z in list(when_names.keys())
    ], ids=[format_test_name(place, how, when) for place, how, when in
            itertools.product(place_names.keys(), how_names.keys(), when_names.keys())])
    def test_checking_multiple_select(self, driver, place, how, when):
        multiple_selector_page = MultipleSelectsInputPage(driver, self.PAGE_URL)
        multiple_selector_page.open()
        multiple_selector_page.is_opened(self.PAGE_URL)
        place_select_name = multiple_selector_page.choose_place_select(place)
        how_select_name = multiple_selector_page.choose_how_select(how)
        when_select_name = multiple_selector_page.choose_when_select(when)
        multiple_selector_page.click_submit()
        result_text = multiple_selector_page.get_result_text()
        assert f'to go by {how_select_name} to the {place_select_name} {when_select_name}' == result_text
