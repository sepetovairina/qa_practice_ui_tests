import string
from faker import Faker

place_names = {
    1: "Sea",
    2: "Mountains",
    3: "Old town",
    4: "Ocean",
    5: "Restaurant"
}

how_names = {
    1: "Car",
    2: "Bus",
    3: "Train",
    4: "Air"
}

when_names = {
    1: "Today",
    2: "Tomorrow",
    3: "Next week"
}


def format_test_name(place, how, when):
    place_name = place_names.get(place, "Unknown")
    how_name = how_names.get(how, "Unknown")
    when_name = when_names.get(when, "Unknown")
    return f"{place_name}, {how_name}, {when_name}"


class DataGenerator:
    def __init__(self):
        self.fake = Faker()

    def generator_random_text(self, length):
        allowed_characters = string.ascii_letters + string.digits + '_-'
        return ''.join(self.fake.random_choices(allowed_characters, length))

    def generator_negative_random_text(self, length):
        allowed_characters = '!@#$%^&*()+`~|\\/{}][''":;.,?><'
        return ''.join(self.fake.random_choices(allowed_characters, length))

    def generator_email(self):
        email = self.fake.email()
        return email

    def generator_password(self, length, special_chars=True, digits=True, upper_case=True, lower_case=True):
        password = self.fake.password(length, special_chars, digits, upper_case, lower_case)
        return password
