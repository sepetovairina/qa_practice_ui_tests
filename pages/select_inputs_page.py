import allure
from base.base_page import BasePage
from locators.select_inputs_page_locators import SingleSelectPageLocators, MultipleSelectsPageLocators


class SingleSelectInputPage(BasePage):
    locators = SingleSelectPageLocators()

    def choice_select(self, select_number):
        with allure.step('Get all select list'):
            select_option_list = self.elements_are_present(self.locators.SELECT_OPTIONS)
        with allure.step('Click on the select'):
            self.element_is_visible(self.locators.SELECT).click()
        with allure.step(f'Choose select number {select_number} from select_option_list'):
            self.element_is_clickable(select_option_list[select_number]).click()
        with allure.step('Get select name'):
            select_name = select_option_list[select_number].text
        with allure.step('Click submit button'):
            self.element_is_clickable(self.locators.SUBMIT_BUTTON).click()
        return select_name

    def get_result_text(self):
        with allure.step('Check result is visible'):
            self.element_is_visible(self.locators.RESULT)
        with allure.step('Get result text'):
            result_text = self.element_is_visible(self.locators.RESULT_TEXT).text
        return result_text


class MultipleSelectsInputPage(BasePage):
    locators = MultipleSelectsPageLocators()

    def choice_select(self, select_option, select_name, select_number):
        with allure.step('Get all select list'):
            select_option_list = self.elements_are_present(select_option)
        with allure.step('Click on the select'):
            self.element_is_visible(select_name).click()
        with allure.step(f'Choose select number {select_number} from select_option_list'):
            self.element_is_clickable(select_option_list[select_number]).click()
        with allure.step('Get select name'):
            choose_select_name_text = select_option_list[select_number].text.lower()
        return choose_select_name_text

    @allure.step('Choose place select')
    def choose_place_select(self, select_number):
        option = self.locators.PLACE_SELECT_OPTIONS
        name = self.locators.PLACE_SELECT
        choose_place_select_name_text = self.choice_select(option, name, select_number)
        return choose_place_select_name_text

    @allure.step('Choose how select')
    def choose_how_select(self, select_number):
        option = self.locators.HOW_SELECT_OPTIONS
        name = self.locators.HOW_SELECT
        choose_how_select_name_text = self.choice_select(option, name, select_number)
        return choose_how_select_name_text

    @allure.step('Choose when select')
    def choose_when_select(self, select_number):
        option = self.locators.WHEN_SELECT_OPTIONS
        name = self.locators.WHEN_SELECT
        choose_when_select_name_text = self.choice_select(option, name, select_number)
        return choose_when_select_name_text

    def click_submit(self):
        with allure.step('Click submit button'):
            self.element_is_clickable(self.locators.SUBMIT_BUTTON).click()

    def get_result_text(self):
        with allure.step('Check result is visible'):
            self.element_is_visible(self.locators.RESULT)
        with allure.step('Get result text'):
            result_text = self.element_is_visible(self.locators.RESULT_TEXT).text
        return result_text
