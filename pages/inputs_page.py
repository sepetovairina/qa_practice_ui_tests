import allure
from selenium.common import TimeoutException
from selenium.webdriver import Keys
from base.base_page import BasePage
from locators.input_page_locators import TextInputPageLocators, EmailFieldPageLocators, PasswordFieldPageLocators


class TextInputPage(BasePage):
    locators = TextInputPageLocators()

    def enter_text(self, random_text):
        with allure.step("Enter valid random text into input"):
            self.element_is_clickable(self.locators.TEXT_INPUT).send_keys(random_text)
        with allure.step("Pressing the 'Enter' button'"):
            self.element_is_clickable(self.locators.TEXT_INPUT).send_keys(Keys.ENTER)
        with allure.step("Result is displayed"):
            self.element_is_visible(self.locators.RESULT)
        with allure.step("Get the output text"):
            output_text = self.element_is_visible(self.locators.TEXT_OUTPUT).text
        return output_text

    def negative_enter_text(self, random_text):
        with allure.step("Enter invalid random text into input"):
            self.element_is_clickable(self.locators.TEXT_INPUT).send_keys(random_text)
        with allure.step("Pressing the 'Enter' button'"):
            self.element_is_clickable(self.locators.TEXT_INPUT).send_keys(Keys.ENTER)
        with allure.step("Get the error text"):
            invalid_text_element = self.element_is_visible(self.locators.INVALID_TEXT)
        try:
            invalid_text_2_element = self.element_is_visible(self.locators.INVALID_TEXT_2)
            with allure.step("Get the error2 text"):
                invalid_text_2 = invalid_text_2_element.text
        except TimeoutException:
            invalid_text_2 = None
        invalid_text = invalid_text_element.text if invalid_text_element else None
        return invalid_text, invalid_text_2


class EmailFieldPage(BasePage):
    locators = EmailFieldPageLocators()

    def enter_email(self, email):
        with allure.step("Enter valid email into input"):
            self.element_is_clickable(self.locators.EMAIL_INPUT).send_keys(email)
        with allure.step("Pressing the 'Enter' button'"):
            self.element_is_clickable(self.locators.EMAIL_INPUT).send_keys(Keys.ENTER)
        with allure.step("Result is displayed"):
            self.element_is_visible(self.locators.RESULT)
        with allure.step("Get the output email"):
            email_output = self.element_is_visible(self.locators.EMAIL_OUTPUT).text
        return email_output

    def enter_invalid_email(self, random_text):
        with allure.step("Enter invalid email into input"):
            self.element_is_clickable(self.locators.EMAIL_INPUT).send_keys(random_text)
        with allure.step("Pressing the 'Enter' button'"):
            self.element_is_clickable(self.locators.EMAIL_INPUT).send_keys(Keys.ENTER)
        with allure.step("Get the error text"):
            invalid_email_text = self.element_is_visible(self.locators.INVALID_EMAIL).text
        return invalid_email_text


class PasswordFieldPage(BasePage):
    locators = PasswordFieldPageLocators()

    def enter_password(self, password):
        with allure.step("Enter valid password into input"):
            self.element_is_clickable(self.locators.PASSWORD_INPUT).send_keys(password)
        with allure.step("Pressing the 'Enter' button'"):
            self.element_is_clickable(self.locators.PASSWORD_INPUT).send_keys(Keys.ENTER)
        with allure.step("Result is displayed"):
            self.element_is_visible(self.locators.RESULT)
        with allure.step("Get the output password"):
            password_output = self.element_is_visible(self.locators.PASSWORD_OUTPUT).text
        return password_output

    def enter_invalid_password(self, password):
        with allure.step("Enter invalid password into input"):
            self.element_is_clickable(self.locators.PASSWORD_INPUT).send_keys(password)
        with allure.step("Pressing the 'Enter' button'"):
            self.element_is_clickable(self.locators.PASSWORD_INPUT).send_keys(Keys.ENTER)
        with allure.step("Get the error text"):
            invalid_password_text = self.element_is_visible(self.locators.INVALID_PASSWORD).text
        return invalid_password_text
