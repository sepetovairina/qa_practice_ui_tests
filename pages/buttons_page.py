import allure
from base.base_page import BasePage
from locators.buttons_page_locators import SimpleButtonPageLocators, LooksLikeAButtonPageLocators, \
    DisabledButtonPageLocators


class SimpleButtonPage(BasePage):
    locators = SimpleButtonPageLocators()

    def click_simple_button(self):
        with allure.step('Click simple button'):
            self.element_is_clickable(self.locators.SIMPLE_BUTTON).click()
        with allure.step('Get simple button text'):
            simple_button_text = self.element_is_clickable(self.locators.SIMPLE_BUTTON).get_attribute('value')
        with allure.step('Get result text'):
            result_text = self.element_is_visible(self.locators.RESULT_TEXT).text
        return simple_button_text, result_text


class LooksLikeAButtonPage(BasePage):
    locators = LooksLikeAButtonPageLocators()

    def click_looks_like_a_button(self):
        with allure.step('Click looks like a button'):
            self.element_is_clickable(self.locators.LOOKS_LIKE_A_BUTTON).click()
        with allure.step('Get looks like a button text'):
            looks_like_a_button_text = self.element_is_clickable(self.locators.LOOKS_LIKE_A_BUTTON).text
        with allure.step('Get result text'):
            result_text = self.element_is_visible(self.locators.RESULT_TEXT).text
        return looks_like_a_button_text, result_text


class DisabledButtonPage(BasePage):
    locators = DisabledButtonPageLocators()

    def check_disabled_button(self):
        select_state_attribute = self.element_is_clickable(self.locators.SELECT_STATE).get_attribute(
            'data-gtm-form-interact-field-id')
        with allure.step('Get select state text'):
            select_state_text = self.element_is_present(self.locators.DISABLE_STATE).text
        with allure.step('Checking that the "Submit" button is present on the page'):
            submit_state = self.element_is_present(self.locators.SUBMIT_BUTTON)
        with allure.step('Checking that the "Submit" button is not active'):
            if submit_state.is_enabled():
                raise AssertionError('"Submit" button is active')
        return select_state_attribute, select_state_text, submit_state.get_attribute('disabled')

    def click_enabled_button(self):
        with allure.step('Click on the drop-down list'):
            self.element_is_clickable(self.locators.SELECT_STATE).click()
        with allure.step('Click on the enable state'):
            self.element_is_present(self.locators.ENABLED_STATE).click()
        with allure.step('Click on the submit button'):
            self.element_is_clickable(self.locators.SUBMIT_BUTTON).click()
        with allure.step('Get result text'):
            result_text = self.element_is_visible(self.locators.RESULT_TEXT).text
        return result_text

