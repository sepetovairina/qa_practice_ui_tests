import allure
from base.base_page import BasePage
from locators.checkbox_page_locators import SingleCheckboxPageLocators, CheckboxesPageLocators


class SingleCheckboxPage(BasePage):
    locators = SingleCheckboxPageLocators()

    def check_checkbox_on(self):
        with allure.step('Get checkbox text'):
            checkbox_text = self.element_is_visible(self.locators.CHECKBOX_TEXT).text
        with allure.step('Click on the checkbox'):
            self.element_is_clickable(self.locators.CHECKBOX).click()
        with allure.step('Click on the submit button'):
            self.element_is_clickable(self.locators.SUBMIT_BUTTON).click()
        with allure.step('Checking the visibility of the result'):
            self.element_is_visible(self.locators.RESULT)
        with allure.step('Get result text'):
            result_text = self.element_is_visible(self.locators.RESULT_TEXT).text
        return checkbox_text, result_text

    def check_checkbox_off(self):
        with allure.step('Get checkbox text'):
            checkbox_text = self.element_is_visible(self.locators.CHECKBOX_TEXT).text
        with allure.step('Checking the clickability of a checkbox'):
            self.element_is_clickable(self.locators.CHECKBOX)
        with allure.step('Click on the submit button'):
            self.element_is_clickable(self.locators.SUBMIT_BUTTON).click()
        element_result = self.find_all(self.locators.RESULT)
        return checkbox_text, element_result


class CheckboxesPage(BasePage):
    locators = CheckboxesPageLocators()

    def click_on_the_checkboxes(self, number, checkboxes_indexes):
        list_checkbox_names = []
        with allure.step('Get all checkboxes list'):
            checkboxes_list = self.elements_are_visible(self.locators.CHECKBOX)
        with allure.step('Get all checkboxes names list'):
            checkboxes_names_list = self.elements_are_visible(self.locators.CHECKBOX_NAME)
        with allure.step('Click on the checkbox(es)'):
            for index in checkboxes_indexes:
                current_index = number + index
                self.element_is_clickable(checkboxes_list[current_index]).click()
                list_checkbox_names.append(checkboxes_names_list[current_index].text.lower())
        with allure.step('Get a list of names of selected checkboxes'):
            checkbox_names = ', '.join(list_checkbox_names)
        with allure.step('Click on the submit button'):
            self.element_is_clickable(self.locators.SUBMIT_BUTTON).click()
        with allure.step('Checking the visibility of the result'):
            self.element_is_visible(self.locators.RESULT)
        with allure.step('Get result text'):
            result_text = self.element_is_visible(self.locators.RESULT_TEXT).text
        return checkbox_names, result_text

