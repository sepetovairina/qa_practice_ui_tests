import allure
from base.base_page import BasePage
from locators.new_tab_page_locators import NewTabPageLocators


class NewTabPage(BasePage):
    locators = NewTabPageLocators()

    @allure.step('Click on new tab link')
    def click_new_tab_link(self):
        self.element_is_clickable(self.locators.LINK).click()

    @allure.step('Switching to new tab')
    def switching_to_a_new_tab(self):
        self.switch_to_tab(-1)

    @allure.step('Checking for opening a new tab')
    def check_new_tab_link(self, url):
        self.check_url(url)

    @allure.step('Click on new tab button')
    def click_new_tab_button(self):
        self.element_is_clickable(self.locators.BUTTON_LINK).click()
