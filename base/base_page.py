import allure
from selenium.webdriver.support.wait import WebDriverWait as wait
from selenium.webdriver.support import expected_conditions as EC


class BasePage:
    def __init__(self, driver, url):
        self.driver = driver
        self.url = url
        self.wait = wait(driver, 10, poll_frequency=1)

    def open(self):
        with allure.step(f'Open {self.url}'):
            self.driver.get(self.url)

    def is_opened(self, url):
        with allure.step(f'Checking that {self.url} is opened'):
            self.wait.until(EC.url_to_be(url))

    def find(self, args):
        return self.driver.find_element(*args)

    def find_all(self, args):
        return self.driver.find_elements(*args)

    def element_is_clickable(self, locator, timeout=5):
        return wait(self.driver, timeout).until(EC.element_to_be_clickable(locator))

    def element_is_visible(self, locator, timeout=5):
        return wait(self.driver, timeout).until(EC.visibility_of_element_located(locator))

    def elements_are_visible(self, locator, timeout=5):
        return wait(self.driver, timeout).until(EC.visibility_of_all_elements_located(locator))

    def element_is_present(self, locator, timeout=5):
        return wait(self.driver, timeout).until(EC.presence_of_element_located(locator))

    def elements_are_present(self, locator, timeout=5):
        return wait(self.driver, timeout).until(EC.presence_of_all_elements_located(locator))

    def switch_to_tab(self, tab_index):
        self.driver.switch_to.window(self.driver.window_handles[tab_index])

    def check_url(self, url, timeout=5):
        return wait(self.driver, timeout).until(EC.url_to_be(url))
