class SimpleButtonPageLocators:
    SIMPLE_BUTTON = ('id', 'submit-id-submit')
    RESULT_TEXT = ('id', 'result-text')


class LooksLikeAButtonPageLocators:
    LOOKS_LIKE_A_BUTTON = ('xpath', "//a[@class='a-button']")
    RESULT_TEXT = ('id', 'result-text')


class DisabledButtonPageLocators:
    SELECT_STATE = ('id', 'id_select_state')
    DISABLE_STATE = ('xpath', '//option[@value="disabled"]')
    ENABLED_STATE = ('xpath', '//option[@value="enabled"]')
    SUBMIT_BUTTON = ('id', 'submit-id-submit')
    RESULT_TEXT = ('id','result-text')

