
class SingleSelectPageLocators:
    SELECT = ('tag name', 'select')
    SELECT_OPTIONS = ('xpath', '//option[@value]')
    SUBMIT_BUTTON = ('id', 'submit-id-submit')
    RESULT = ('id', 'result')
    RESULT_TEXT = ('id', 'result-text')


class MultipleSelectsPageLocators:
    PLACE_SELECT = ('id', 'id_choose_the_place_you_want_to_go')
    PLACE_SELECT_OPTIONS = ('xpath', '//select[@id="id_choose_the_place_you_want_to_go"]/option[@value]')
    HOW_SELECT = ('id', 'id_choose_how_you_want_to_get_there')
    HOW_SELECT_OPTIONS = ('xpath', '//select[@id="id_choose_how_you_want_to_get_there"]/option[@value]')
    WHEN_SELECT = ('id', 'id_choose_when_you_want_to_go')
    WHEN_SELECT_OPTIONS = ('xpath', '//select[@id="id_choose_when_you_want_to_go"]/option[@value]')
    SUBMIT_BUTTON = ('id', 'submit-id-submit')
    RESULT = ('id', 'result')
    RESULT_TEXT = ('id', 'result-text')
