class TextInputPageLocators:
    TEXT_INPUT = ('id', 'id_text_string')
    RESULT = ('xpath', "//p[text()='Your input was:']")
    TEXT_OUTPUT = ('id', "result-text")
    INVALID_TEXT = ('id', "error_1_id_text_string")
    INVALID_TEXT_2 = ('id', "error_2_id_text_string")


class EmailFieldPageLocators:
    EMAIL_INPUT = ('id', 'id_email')
    RESULT = ('xpath', "//p[text()='Your input was:']")
    EMAIL_OUTPUT = ('id', "result-text")
    INVALID_EMAIL = ('id', "error_1_id_email")


class PasswordFieldPageLocators:
    PASSWORD_INPUT = ('id', 'id_password')
    RESULT = ('xpath', "//p[text()='Your input was:']")
    PASSWORD_OUTPUT = ('id', 'result-text')
    INVALID_PASSWORD = ('id', 'error_1_id_password')

