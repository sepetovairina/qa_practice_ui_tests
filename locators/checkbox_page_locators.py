class SingleCheckboxPageLocators:
    CHECKBOX = ('id', 'id_checkbox_0')
    CHECKBOX_TEXT = ('xpath', '//label[@class="form-check-label"]')
    SUBMIT_BUTTON = ('id', 'submit-id-submit')
    RESULT = ('id', 'result')
    RESULT_TEXT = ('id', 'result-text')


class CheckboxesPageLocators:
    CHECKBOX = ('xpath', '//input[@type="checkbox"]')
    CHECKBOX_NAME = ('xpath', "//label[@class='form-check-label']")
    SUBMIT_BUTTON = ('id', 'submit-id-submit')
    RESULT = ('id', 'result')
    RESULT_TEXT = ('id', 'result-text')